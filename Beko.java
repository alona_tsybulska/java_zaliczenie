
public class Beko extends Pralka {
	private MarkiPralek marka;

	public Beko() {
		super();
		this.marka = MarkiPralek.BEKO;
	}

	@Override
	public String getMarka() {
		return MarkiPralek.BEKO.name();
	}

	public void setMarka(MarkiPralek marka) {
		this.marka = marka;
	}

	@Override
	public void tempUp() throws MojWyjatek {
		if (getTemp() < 90) {
			setTemp(getTemp() + 1);
		} else {
			throw new MojWyjatek("Temperatura jest poza zakresem!");
		}
		System.out.println("Ustawiono temperature: " + getTemp() + " ºC");
	}

	@Override
	public void tempDown() throws MojWyjatek {
		if (getTemp() > 0) {
			setTemp(getTemp() - 1);
		} else {
			throw new MojWyjatek("Temperatura jest poza zakresem!");
		}
		System.out.println("Ustawiono temperature: " + getTemp() + " ºC");
	}

}
