import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) throws MojWyjatek {
		Pralka beko =  new Beko();
		Pralka whirpool =  new Whirpool();
		Pralka amica =  new Amica();
		List <Pralka> listaPralek =  new ArrayList<> ();
		
		listaPralek.add(beko);
		listaPralek.add(whirpool);
		listaPralek.add(amica);
		
		//beko 
		beko.setPredkosc(100);
		beko.setProgramPrania(1);
		beko.setTemp(43.6);
		System.out.println(beko.showStatus());
		beko.tempUp();
		beko.downPredkosc();
		beko.previusProgram();
		System.out.println(beko.showStatus());
		
		//whirpoll
		whirpool.setPredkosc(900);
		whirpool.setProgramPrania(25);
		whirpool.setTemp(55);
		System.out.println(whirpool.showStatus());
		whirpool.upPredkosc();
		whirpool.tempUp();
		whirpool.nextProgram();
		System.out.println(whirpool.showStatus());
		
		//amica
		amica.setPredkosc(1000);
		amica.setProgramPrania(20);
		amica.setTemp(63.222);
		System.out.println(amica.showStatus());
		amica.downPredkosc();
		amica.tempUp();
		amica.nextProgram();
		System.out.println(amica.showStatus());
		
		//sortowanie
		Pralka.sortedList(listaPralek);
	}
}
