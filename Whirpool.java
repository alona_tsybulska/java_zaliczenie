
public class Whirpool extends Pralka {

	private MarkiPralek marka;

	public Whirpool() {
		super();
		this.marka = MarkiPralek.WIRPOOL;
	}

	public String getMarka() {
		return MarkiPralek.WIRPOOL.name();
	}

	public void setMarka(MarkiPralek marka) {
		this.marka = marka;
	}

	@Override
	public int setProgramPrania(int programPrania) throws MojWyjatek {
		if (programPrania > 0 && programPrania <= 25) {
			this.programPrania = programPrania;
		} else
			throw new MojWyjatek("Brak programu.Wybierz program między 1 a 25.");
		return programPrania;

	}

	@Override
	public void nextProgram() throws MojWyjatek {
		if (getProgramPrania() < 25) {
			setProgramPrania(getProgramPrania() + 1);
		} else if (getProgramPrania() == 25) {
			setProgramPrania(1);
		}

	}
    @Override
	public void previusProgram() throws MojWyjatek {

		if (getProgramPrania() > 1) {
			setProgramPrania(getProgramPrania() - 1);
		} else if (getProgramPrania() == 1) {
			setProgramPrania(25);
		}
	}

}
