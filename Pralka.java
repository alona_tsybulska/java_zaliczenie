import java.util.List;

public abstract class Pralka {

	protected int programPrania;
	private double temp;
	private int predkosc;

	public Pralka() {

	}

	public abstract String getMarka();

	public int getProgramPrania() {
		return programPrania;
	}

	public int setProgramPrania(int programPrania) throws MojWyjatek {
		if (programPrania > 0 && programPrania <= 20) {
			this.programPrania = programPrania;
		} else
			throw new MojWyjatek("Brak programu.Wybierz program miedzy 1 a 20.");
		return programPrania;

	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp)  throws MojWyjatek{
		if (temp >= 0 && temp <= 90) {
			this.temp = temp;
		} else throw new MojWyjatek("Podana temperatura jest poza zakresem.Wybierz temperature z zakresu 0-90");
		
		 this.temp = (double) Math.round(getTemp() * 2) / 2;

	}

	public int getPredkosc() {
		return predkosc;
	}

	public void setPredkosc(int predkosc) throws MojWyjatek {
		if (predkosc >= 0 && predkosc <= 1000) {
			this.predkosc = predkosc;
		}else throw new MojWyjatek("wybrana ilosc obrotow jest poza zakresem" );
	}

	public void nextProgram() throws MojWyjatek {
		if (getProgramPrania() < 20) {
			setProgramPrania(getProgramPrania() + 1);
		} else if (getProgramPrania() == 20) {
			setProgramPrania(1);
		}

	}

	public void previusProgram() throws MojWyjatek {
	
		if (getProgramPrania() > 1) {
			setProgramPrania(getProgramPrania() - 1);
		} else if (getProgramPrania() == 1) {
			setProgramPrania(20);
		}
	}

	public void tempUp() throws MojWyjatek {
		if (getTemp() < 90) {
			setTemp(getTemp() + 0.5);
		} else {
			throw new MojWyjatek("Temperatura jest poza zakresem!");
		}
		System.out.println("Ustawiono temperature: " + getTemp() + " ºC");
	}

	public void tempDown() throws MojWyjatek {
		if (getTemp() > 0) {
			setTemp(getTemp() - 0.5);
		} else {
			throw new MojWyjatek("Temperatura jest poza zakresem!");
		}
		System.out.println("Ustawiono temperature: " + getTemp() + " ºC");
	}

	public void upPredkosc() throws MojWyjatek {
		if (getPredkosc() < 1000) {
			setPredkosc(getPredkosc() + 100);
		} else if (getPredkosc() == 1000) {
			setPredkosc(0);
		}
	}

	public void downPredkosc() throws MojWyjatek {
		if (getPredkosc() > 0) {
			setPredkosc(getPredkosc() - 100);
		} else if (getPredkosc() == 0) {
			setPredkosc(1000);
		}
	}

	public String showStatus() {
		return " Model pralki: " + getMarka() + " Wybrano program: " + getProgramPrania() + ", temperatura: " + getTemp()
				+ ". Ilosc obrotow: " + getPredkosc();

	}

	public static void sortedList(List<Pralka> listaPralek) {
		listaPralek.stream().forEach(pralka -> System.out.println(pralka.showStatus()));

		System.out.println("--------------------------------------------------------");

		listaPralek.stream().sorted((pralka1, pralka2) -> pralka1.getMarka().compareTo(pralka2.getMarka()))
				.forEach(pralka -> System.out.println(pralka.showStatus()));
	}

}
