
public class Amica extends Pralka {
	private MarkiPralek marka;

	public Amica() {
		super();
		this.setMarka(MarkiPralek.AMICA);
	}

	@Override
	public String getMarka() {
		return MarkiPralek.AMICA.name();
	}

	public void setMarka(MarkiPralek marka) {
		this.marka = marka;
	}

}
